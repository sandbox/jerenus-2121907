<?php

/**
 * @file
 * Contains administrative pages for creating, editing, and deleting queues.
 */

/**
 * Flag administration page. Display a list of existing queues.
 */
function queue_admin_page() {
  $queue_type = QueueTypeEntityCRUD::queue_type_new('333fdg333', 'bisdbisd', 'sdsd');
  
  QueueTypeEntityCRUD::queue_type_save($queue_type);
  dsm($queue_type);
//  $xx = QueueTypeEntityCRUD::queue_type_load('xxxxx');
//  dsm($xx);
  return '$queue_type';
}

function queue_type_form($form, &$form_state, $queue_type_entity, $op, $entity_type) {
//  dsm($form_state);
//  dsm($queue_type_entity);
//  dsm($op);
//  dsm($entity_type);
 
  $is_a_copy = FALSE;
  
  switch ($op) {
    case 'add':

      break;
    case 'edit':

      break;
    case 'clone':
      
      $queue_type_entity->label = 'Clone of ' . $queue_type_entity->label;
      $is_a_copy = TRUE;
      break;

    default:
      break;
  }
  
  $is_new = isset($queue_type_entity->is_new) && $queue_type_entity->is_new;
  $options = drupal_json_decode($queue_type_entity->options);

  // Make the type object available to implementations of hook_form_alter.
  $form['#queue_name'] = $entity_type;

  $form['label'] = array(
      '#title' => t('Title'),
      '#type' => 'textfield',
      '#default_value' => $is_new && !$is_a_copy ? '' : $queue_type_entity->label,
      '#description' => t('The human-readable name of this queue type. This text will be displayed as part of the list on the <em>Add new queue</em> page. It is recommended that this name begin with a capital letter and contain only letters, numbers, and spaces. This name must be unique.'),
      '#required' => TRUE,
      '#size' => 30,
  );

  $form['type'] = array(
      '#type' => 'machine_name',
      '#maxlength' => 32,
      '#machine_name' => array(
        'exists' => 'queue_type_load',
        'source' => array('label'),
      ),
      '#description' => t('A unique machine-readable name for this queue type. It must only contain lowercase letters, numbers, and underscores. This name will be used for constructing the URL of the %node-add page, in which underscores will be converted into hyphens.', array(
          '%queue-add' => t('Add new queue'),
      )),
  );

  $form['description'] = array(
      '#title' => t('Description'),
      '#type' => 'textarea',
      '#default_value' => isset($options['description']) ? $options['description'] : '',
      '#description' => t('Describe this queue type. The text will be displayed on the <em>Add new queue</em> page.'),
  );

  $form['actions'] = array(
      '#type' => 'actions',
  );

  $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save queue'),
      // We put this button on the form before calling $queue->options_form()
      // to give the queue handler a chance to remove it (e.g. queue_broken).
      '#weight' => 999,
  );

  return $form;
}

function queue_type_form_validate($form, &$form_state) {
  
    // Ensure a safe machine name.
    if (!preg_match('/^[a-z_][a-z0-9_]*$/', $form_state['input']['type'])) {
      form_set_error('queue_type_characters', t('The queue name may only contain lowercase letters, underscores, numbers, and cannot start with numbers.'));
    }
    else {
    }
    return TRUE;
}

function queue_type_form_submit($form, &$form_state) {
  
  $label = $form_state['input']['label'];
  $type = $form_state['input']['type'];
  $description = $form_state['input']['description'];
  
  $options = array();
  $options['description'] = $description;

  $options = drupal_json_encode($options);  
  
  $queue_type = new stdClass();
  
  switch ($form_state['op']) {
    case 'add':
    case 'clone':
      $queue_type = QueueTypeEntityCRUD::queue_type_new($label, $type, $options);
      $wrapper = entity_metadata_wrapper('queue_type', $queue_type);
      $wrapper->save();

      break;
    case 'edit':
      

      break;

    default:
      break;
  }
  
  //重定向到管理页面
  $form_state['redirect'] = '/admin/structure/queue-types';
}

/**
 * Load queue Type.
 */
function queue_type_load($type_name) {
  return QueueTypeEntityCRUD::queue_types($type_name);
}