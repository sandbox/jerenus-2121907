<?php

/**
 * @file
 * Provides supporting code for the entity/fields system.
 *
 * Note: We're making the <em>queueings</em> fieldable, not the <em>queues</em>.
 * (In the same way that Drupal makes <em>nodes</em> fieldable, not <em>node
 * types</em>).
 */

/**
 * QueueingEntity class.
 */
class QueueingEntity extends Entity {

  protected function defaultLabel() {
    return $this->title;
  }

  protected function defaultUri() {
    return array('path' => 'queue/' . $this->identifier());
  }

}

class QueueingController extends EntityAPIController {

  public function create(array $values = array()) {
    global $user;
    $values += array(
        'timestamp' => REQUEST_TIME,
        'uid' => $user->uid,
    );
    return parent::create($values);
  }

  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    $wrapper = entity_metadata_wrapper('queueing', $entity);
    $content['author'] = array('#markup' => t('Created by: !author', array('!author' => $wrapper->uid->name->value(array('sanitize' => TRUE)))));

    // Make Description and Status themed like default fields.
    $content['description'] = array(
        '#theme' => 'field',
        '#weight' => 0,
        '#title' => t('Description'),
        '#access' => TRUE,
        '#label_display' => 'above',
        '#view_mode' => 'full',
        '#language' => LANGUAGE_NONE,
        '#field_name' => 'field_fake_description',
        '#field_type' => 'text',
        '#entity_type' => 'queueing',
        '#bundle' => $entity->type,
        '#items' => array(array('value' => $entity->description)),
        '#formatter' => 'text_default',
        0 => array('#markup' => check_plain($entity->description))
    );

    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }

}

/**
 * EntityTest Views Controller class.
 */
class QueueingViewsController extends EntityDefaultViewsController {
  /**
   * Edit or add extra fields to views_data().
   */
  public function views_data() {
    $data = parent::views_data();
    // Add your custom data here
    /*
    * Example: change the handler of a field
    * if the 'created' field is a unix timestamp in the database,
    * Entity API will set the handler to views_handler_field_numeric,
    * change this to the views date handler
    */
    $data['queueing']['timestamp']['field']['handler'] = 'views_handler_field_date';
    return $data;
  }
}

/**
 * This class is define QueueingEntity's CRUD.
 */
class QueueingEntityCRUD {

  /**
   * Load a queueing.
   */
  static public function queueing_load($queueing_id, $reset = FALSE) {
    $queueings = queueing_load_multiple(array($queueing_id), array(), $reset);
    return reset($queueings);
  }

  /**
   * Load multiple queueings based on certain conditions.
   */
  static public function queueing_load_multiple($queueing_ids = array(), $conditions = array(), $reset = FALSE) {
    return entity_load('queueing', $queueing_ids, $conditions, $reset);
  }

  /**
   * Save queueing.
   */
  static public function queueing_save($queueing) {
    entity_save('queueing', $queueing);
  }

  /**
   * Delete single queueing.
   */
  static public function queueing_delete($queueing) {
    entity_delete('queueing', entity_id('queueing', $queueing));
  }

  /**
   * Delete multiple queueings.
   */
  static public function queueing_delete_multiple($queueing_ids) {
    entity_delete_multiple('queueing', $queueing_ids);
  }

}
