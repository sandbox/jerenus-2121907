<?php

/**
 * @file
 * Provides supporting code for the entity/fields system.
 *
 * Note: We're making the <em>queueings</em> fieldable, not the <em>queues</em>.
 * (In the same way that Drupal makes <em>nodes</em> fieldable, not <em>node
 * types</em>).
 */

/**
 * Task Type class.
 */
class QueueTypeEntity extends Entity {

  public $type;
  public $label;
  public $weight = 0;

  public function __construct($values = array()) {
    parent::__construct($values, 'queue_type');
  }

  function isLocked() {
    return isset($this->status) && empty($this->is_new) && (($this->status & ENTITY_IN_CODE) || ($this->status & ENTITY_FIXED));
  }

}

class QueueTypeController extends EntityAPIControllerExportable {

  public function create(array $values = array()) {
    $values += array(
        'label' => '',
        'options' => '',
    );
    return parent::create($values);
  }

  /**
   * Save Task Type.
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {
    parent::save($entity, $transaction);
    // Rebuild menu registry. We do not call menu_rebuild directly, but set
    // variable that indicates rebuild in the end.
    // @see _http://drupal.org/node/1399618
    variable_set('menu_rebuild_needed', TRUE);
  }

  public function delete($ids, DatabaseTransaction $transaction = NULL) {
    parent::delete($ids, $transaction);
    // Rebuild menu registry. We do not call menu_rebuild directly, but set
    // variable that indicates rebuild in the end.
    // @see _http://drupal.org/node/1399618
    variable_set('menu_rebuild_needed', TRUE);
  }

}

/**
 * UI controller - may be put in any include file and loaded via the code registry.
 */
class QueueTypeUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['description'] = 'Manage queue types, including fields.';
    
    return $items;
  }

}

/**
 * EntityTest Views Controller class.
 */
class QueueTypeViewsController extends EntityDefaultViewsController {
  /**
   * Edit or add extra fields to views_data().
   */
  public function views_data() {
    $data = parent::views_data();
    // Add your custom data here
    /*
    * Example: change the handler of a field
    * if the 'created' field is a unix timestamp in the database,
    * Entity API will set the handler to views_handler_field_numeric,
    * change this to the views date handler
    */
    //$data['entity_test']['created']['field']['handler'] = 'views_handler_field_date';
    return $data;
  }
}

/**
 * This class is define QueueTypeEntity's CRUD.
 */
class QueueTypeEntityCRUD {

  /**
   * List of queue Types.
   */
  static public function queue_types($type_name = NULL) {
    $types = entity_load_multiple_by_name('queue_type', isset($type_name) ? array($type_name) : FALSE);
    return isset($type_name) ? reset($types) : $types;
  }

  /**
   * Create queue type entity.
   */
  static public function queue_type_new($type, $label, $options) {
    $values = array(
        'type' => $type,
        'label' => $label,
        'options' => $options,
    );
    return entity_create('queue_type', $values);
  }

  /**
   * Save queue type entity.
   */
  static public function queue_type_save($queue_type) {
    entity_save('queue_type', $queue_type);
  }

  /**
   * Delete single case type.
   */
  static public function queue_type_delete($queue_type) {
    entity_delete('queue_type', entity_id('queue_type', $queue_type));
  }

  /**
   * Delete multiple case types.
   */
  static public function queue_type_delete_multiple($queue_type_ids) {
    entity_delete_multiple('queue_type', $queue_type_ids);
  }

}

