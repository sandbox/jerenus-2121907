<?php
/**
 * @file
 * Provides support for Views integration.
 *
 * This exposes queue-related data and declares handlers (fields, filters)
 * and plugins (argument validators).
 */

/**
 * Implements hook_views_data().
 */
function queue_views_data() {
  $data = array();
  // Define the base group of the queue table.  Fields that don't
  // have a group defined will go into this field by default.
//  $data['queue_type']['table']['group'] = t('Queue');
//
//  $data['queue_type']['table']['base'] = array(
//      'field' => 'qid',
//      'title' => t('Queue'),
//      'weight' => -10,
//      'access query tag' => 'node_access',
//      'defaults' => array(
//          'field' => 'label',
//       ),
//  );
//  
//  $data['node']['qid'] = array(
//      'title' => t('Qid'),
//      'help' => t('The Queue IDxxxxxxxxxxx.'),
//      // The help that appears on the UI,
//      // Information for displaying the nid 
//      'field' => array(
//          'handler' => 'views_handler_field_node',
//          'click sortable' => TRUE,
//      ),
//      // Information for accepting a nid as an argument
//      'argument' => array(
//          'handler' => 'views_handler_argument_node_nid',
//          'name field' => 'title',
//          // the field to display in the summary.
//          'numeric' => TRUE, 'validate type' => 'qid',
//      ),
//      // Information for accepting a nid as a filter
//      'filter' => array(
//          'handler' => 'views_handler_filter_numeric',
//      ),
//      // Information for sorting on a nid.
//      'sort' => array(
//          'handler' => 'views_handler_sort',
//      ),
//  );
  
  
//  $data['node']['type'] = array(
//      'title' => t('Queue type'),
//      // The item it appears as on the UI,
//      'help' => t('Queue type.'),
//      // The help that appears on the UI,
//      'field' => array(
//          'handler' => 'views_handler_field_date',
//          'click sortable' => TRUE,
//      ),
//      'sort' => array(
//          'handler' => 'views_handler_sort_date',
//      ), 
//      'filter' => array(
//          'handler' => 'views_handler_filter_date',
//      ),
//  ); 
  
//  $data['node']['type'] = array(
//      'field' => array('title' => t('Type'),
//          'help' => t('The Type of this queue.'),
//          'handler' => 'views_handler_field_node_path',
//      ),
//  );

  return $data;
}

/**
 * Implements hook_views_handlers().
 *
 * Register information about all of the views handlers provided by the Signup
 * module.
 */
//function queue_views_handlers() {
//  return array(
//    'info' => array(
//      'path' => drupal_get_path('module', 'queue') . '/views/handlers',
//    ),
//    'handlers' => array(
//        
//    ),
//  );
//}
//
///**
// * Implements hook_views_plugins().
// */
//function queue_views_plugins() {
//  $path = drupal_get_path('module', 'queue') . '/views/plugins';
//  return array(
//      
//  );
//}
//
